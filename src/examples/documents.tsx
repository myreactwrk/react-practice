
import React from 'react';
import './examples.css'


const ExamplesAndDocumentsList = () =><div className={'d-flex justify-content-center flex-column'}>
    <div className={'examples-header'}>Examples And Document List</div>
    <div className={'examples-subheader'}><a className={'examples-link'} href={'http://localhost:3000/home'}>HOME</a></div>
    <div className={'examples-content'}>
    <ul>
        <li> <a target={'_blank'} href={'https://mui.com/material-ui/material-icons/'}><b>Material Icons</b></a></li>
        <li> <a target={'_blank'} href={'https://mui.com/material-ui/'}>Visual Components (MU)</a></li>
        <hr/>
        <li> <a target={'_blank'} href={'https://mui.com/store/?utm_source=marketing&utm_medium=referral&utm_campaign=templates-cta#populars'}>Popular Templates</a></li>
        <li> <a target={'_blank'} href={'https://github.com/codedthemes/mantis-free-react-admin-template/'}>mantis-free-react-admin-template</a></li>
    </ul>
    </div>
    </div>



export default ExamplesAndDocumentsList;
