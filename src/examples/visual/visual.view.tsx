import React from 'react';
// import { Button } from "react-bootstrap";
// import { Stack } from "react-bootstrap";
import Button from '@mui/material/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Badge, Stack, IconButton } from '@mui/material';
import PhotoCamera from '@mui/icons-material/PhotoCamera';

// @see https://mui.com/material-ui/react-button/

import { styled } from '@mui/material/styles';

const CustomButton = styled(Button)({
}) as typeof Button;

const VisualViewExamples = () => {
    return (<>
        <h1>Visual Examples</h1>
        {<div key={'Home'}><a href={'http://localhost:3000/home'}>HOME</a></div>}

        <div className="d-flex justify-content-center align-items-center">
             <Button variant="contained">Hello World</Button>
        </div>

            <div>
                <Stack direction={'row'}  gap={2}>
                  <Button variant={'outlined'}>
                    Button as link
                  </Button>
                  <Button color={'info'} variant={'contained'}>

                    Button as link
                  </Button>
                    <CustomButton color={'success'} onClick={() => { alert("OK") }} >HESSS</CustomButton>
                </Stack>

            </div>


            <Stack className={'v50'}>
            <Button variant="text">Text</Button>
            <Button variant="contained">Contained</Button>
            <Button variant="outlined">Outlined</Button>
            </Stack>

            <div className={'d-flex justify-content-center'} style={{width: '400px', backgroundColor: 'yellow'}}>
            <Stack direction={'column'}>
            <Button color="secondary">Secondary</Button>
            <Button variant="contained" color="success">Success</Button>
            <Button variant="contained" color="error">Error</Button>
            </Stack>
            </div>

            <div>
    <Stack direction="row" alignItems="center" spacing={2}>
      <Button variant="contained" component="label">
        Upload
        <input hidden accept="image/*" multiple type="file" />
      </Button>
      <IconButton color="primary" aria-label="upload picture" component="label">
        <input hidden accept="image/*" type="file" />
        <PhotoCamera />
      </IconButton>
    </Stack>
            </div>
        </>
    );
}

export default VisualViewExamples;
