import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

import api from './provider.api';


import { AnyAction } from "redux";


// @see https://redux-toolkit.js.org/api/createAsyncThunk
// @see https://redux-toolkit.js.org/api/createslice

export const COMPONENT_KEY_NAME = 'provider';

export const load = createAsyncThunk(
  `${COMPONENT_KEY_NAME}/load`,
  async (words: string, thunkAPI) => {
    const response = await api.loadProvider(words);
    return response.data
  }
)

export type AsynStatus = 'idle' | 'pending' | 'succeeded' | 'rejected';

interface InitialState {
  loading: AsynStatus;
  item: { };
}

const initialState: InitialState = {
  loading: 'idle',
  item: {}
}

const componentSlice = createSlice({
  name: COMPONENT_KEY_NAME,
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder.addCase(load.pending, (state, action) => {
      state.loading = 'pending';
    }).addCase(load.fulfilled, (state, action) => {
      state.loading = 'succeeded';
      state.item = action.payload;
    }).addCase(load.rejected, (state, action) => {
      state.item = {};
    })
  }
});

export const providerActions = componentSlice.actions;
export const providerSlice = componentSlice;
export default componentSlice.reducer
