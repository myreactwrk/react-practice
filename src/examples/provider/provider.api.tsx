import axios, { AxiosResponse } from 'axios';

class Api {
    loadProvider = (words: string) => axios.get('https://crossword.worddict.net/?words=' + words);
}

export default new Api();