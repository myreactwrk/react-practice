import React from 'react';
import { useAppDispatch } from "src/main/store";
import 'bootstrap/dist/css/bootstrap.min.css';

const ProviderView = () => {
    const dispatch = useAppDispatch();
    return (<div className="my-5 mx-5">
        <h1>ProviderView</h1>
            {<div key={'Home'}><a href={'http://localhost:3000/home'}>HOME</a></div>}
        </div>
    );
}

export default ProviderView;
