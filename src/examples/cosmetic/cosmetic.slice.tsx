import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

import api from './cosmetic.api';

import { CosmeticModel } from "./cosmetic.model";
import { AnyAction } from "redux";


// @see https://redux-toolkit.js.org/api/createAsyncThunk
// @see https://redux-toolkit.js.org/api/createslice
// @see https://redux.js.org/tutorials/essentials/part-5-async-logic

export const COMPONENT_KEY_NAME = 'crossword';

export const load = createAsyncThunk(
  `${COMPONENT_KEY_NAME}/load`,
  async (words: string, thunkAPI) => {
    const response = await api.crossword(words);
    return response.data
  }
)

export type AsynStatus = 'idle' | 'pending' | 'succeeded' | 'rejected';

interface Cosmetic {
  name: string;
  url: string;
  price: string;
}

interface InitialState {
  status: 'UNDEF' | 'OK' | 'ERR'; // only as an example here
  loading: AsynStatus;
  item: CosmeticModel;
  age?: number;
  name: any;
  goods: Cosmetic[];
  counter: number;
}

export const initialState: InitialState = {
  status: 'UNDEF',
  loading: 'idle',
  age: 19,
  item: {},
  name: 'Nastya',
  goods: [] as Cosmetic[],
  counter: 0,
}

const isRejectedAction = (action: AnyAction) => action.type.endsWith('rejected');
const isSucceededAction = (action: AnyAction) => action.type.endsWith('succeeded');


const componentSlice = createSlice({
  name: COMPONENT_KEY_NAME,
  initialState,
  reducers: {
    setUser(state, action)  { state.item.user  = action.payload },
    setCount(state, action) { state.item.count = action.payload },
    setAge(state, action)   {state.age = action.payload},
    setName(state, action)   {state.name = action.payload},
    setGoods(state, action)   {state.goods = action.payload},
    setCounter(state, action)   {state.counter = action.payload},
  },
  extraReducers: (builder) => {
    builder.addCase(load.pending, (state, action) => {
      state.loading = 'pending';
    }).addCase(load.fulfilled, (state, action) => {
      state.status = 'OK';
      state.loading = 'succeeded';
      state.item = action.payload;
    }).addCase(load.rejected, (state, action) => {
      state.status = 'OK';
      state.item = {};
    }).addMatcher(
        isRejectedAction,
        (state, action) => { state.status = 'ERR' }
      ).addMatcher(
        isSucceededAction,
        (state, action) => { state.status = 'OK' }
      )
  }
});

export const incrementAsync = (name) => (dispatch) => {
  setTimeout(() => {
    dispatch(componentSlice.actions.setUser(name))
  }, 1000)
}

export const crosswordActions = componentSlice.actions;
export const cosmeticSlice = componentSlice;
export default componentSlice.reducer
