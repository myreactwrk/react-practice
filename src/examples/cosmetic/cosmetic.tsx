import React from 'react';
import {connect} from 'react-redux';
import {AnyAction, bindActionCreators, Dispatch} from 'redux';
import {randomString} from 'src/utils/misc';
import {RootState} from "src/main/store";
import {initialState} from "./cosmetic.slice";
import './cosmetic.css';

import {COMPONENT_KEY_NAME, crosswordActions, incrementAsync, load} from './cosmetic.slice';
import {array} from "yup";

interface ComponentProps extends StateProps, DispatchProps {
}

const ownSettings = {
    version: '0.01'
};

const allGoods = [{name: 'Laneige lip sleeping mask', price: '500 UAH', url: 'https://cdn1.feelunique.com/img/products/171223/laneige_lip_sleeping_mask_mango_20g-1677658622_main.jpg'},
    {name: 'CeraVe Hydrating Cleanser', price: '400 UAH', url: 'https://static.thcdn.com/images/large/original//productimg/1600/1600/11798700-1944898991205302.jpg'},
    {name: 'Vichy Mineral 89 Fortifying And Plumping Daily Booster', price: '700 UAH', url: 'https://i.ebayimg.com/images/g/IbgAAOSwX95g5kGP/s-l500.jpg'},
    {name: 'Petitfee & Koelf Black Pearl&Gold Hydrogel Eye Patch', price: '400 UAH', url: 'https://cdn.shopify.com/s/files/1/0254/3022/9055/products/0719-PETITFEE-Black-Pearl-_-Gold-Hydrogel-Eye-Patch-60-Patches_800x_ec6671a3-e894-4729-a06f-b6e571764ca8.jpg?v=1577871056'},
    {name: 'Cosrx Low Ph Good Morning Gel Cleanser', price: '450 UAH', url: 'https://static.thcdn.com/images/large/original//productimg/1600/1600/11401192-1944895727565704.jpg'},
    {name: '*Laneige lip sleeping mask', price: '500 UAH****', url: 'https://cdn1.feelunique.com/img/products/171223/laneige_lip_sleeping_mask_mango_20g-1677658622_main.jpg'},
    {name: '*CeraVe Hydrating Cleanser', price: '400 UAH****', url: 'https://static.thcdn.com/images/large/original//productimg/1600/1600/11798700-1944898991205302.jpg'},
    {name: '*Vichy Mineral 89 Fortifying And Plumping Daily Booster', price: '700 UAH****', url: 'https://i.ebayimg.com/images/g/IbgAAOSwX95g5kGP/s-l500.jpg'},
    {name: '*Petitfee & Koelf Black Pearl&Gold Hydrogel Eye Patch', price: '400 UAH****', url: 'https://cdn.shopify.com/s/files/1/0254/3022/9055/products/0719-PETITFEE-Black-Pearl-_-Gold-Hydrogel-Eye-Patch-60-Patches_800x_ec6671a3-e894-4729-a06f-b6e571764ca8.jpg?v=1577871056'},
    {name: '*Cosrx Low Ph Good Morning Gel Cleanser', price: '450 UAH****', url: 'https://static.thcdn.com/images/large/original//productimg/1600/1600/11401192-1944895727565704.jpg'}
];

// const getAllCards = () => {
//     const allCards: any[] = [];
//     for (let i = 0; i < initialState.counter; i++) {
//         allCards.push(
//         //     <div key={allGoods[i].name} className="card">
//         //     <div>
//         //         <img className='faceProductPhoto' src={allGoods[i].url}></img>
//         //     </div>
//         //     <div className='container'>
//         //         <h4 className='productInfo'><b>{allGoods[i].name}</b></h4>
//         //         <p className='productInfo'>{allGoods[i].price}</p>
//         //     </div>
//         // </div>
//         )
//     }
//     return allCards;
//
// }

class CrosswordComponent extends React.Component<ComponentProps> {
    componentDidMount() {
        console.log('MOUNT...')
        console.log("############ MOUNT props", this.props);
        this.props.load('hehfulkker,mabafukker,zuzumakker,dudolazer,miniguwer');
    }

    onClick = () => {
        // this.props.incrementAsync('AAA::' + randomString('MX::'));
        // this.props.load('history,listing,conster,fufelesis,bubufukker,mumpakker,babzdakker,recamukker,lulifukker,zizmadufaker,mamgaser')
        // const resWord = 'Anastasia';
        // this.props.setNameee('Sergii');
        const allProducts: any[] = [];
        this.props.setCounter(10);
        for (let i = 0; i < this.props.counter; i++) {
            allProducts.push(allGoods[i]);
        }
        this.props.setGoods(allProducts);
    }

    render() {
        const {item, status, loading} = this.props;

        if (loading !== 'succeeded') {
            return <div>Loading..... status::{status}
                loading: [{loading}]
            </div>
        }

        // @ts-ignore
        return <div>
            <h1>Crossword USER:: [{item.user}] status::{status} Version: {this.props.version}</h1>
            <h1>{initialState.name}</h1>
            <h1>{this.props.name}</h1>
            <h1 id='allCards'>{this.props.goods.map(item => <div key={item.name} className="card">
                <div>
                    <img className='faceProductPhoto' src={item.url}></img>
                </div>
                <div className='container'>
                    <h4 className='productInfo'><b>{item.name}</b></h4>
                    <p className='productInfo'>{item.price}</p>
                </div>
            </div>)}</h1>
            {/*{getAllCards()}*/}

            <div>{<div key={'Home'}><a href={'http://localhost:3000/home'}>HOME</a></div>}</div>
            <h1>
                <div onClick={this.onClick}>Click me to see goods!</div>
            </h1>
            <br/>
            <h1>
                <div onClick={() => {
                    if (this.props.counter > 0) {
                        const allProducts: any[] = [];
                        this.props.setCounter(this.props.counter - 1);
                            for (let i = 0; i < this.props.counter; i++) {
                                allProducts.push(allGoods[i]);
                            }
                        this.props.setGoods(allProducts);
                    }
                }
                }>- Item
                </div>
            </h1>

            <h1>
                <div onClick={() => {
                    if(this.props.counter < allGoods.length + 1) {
                    const allProducts: any[] = [];
                    this.props.setCounter(this.props.counter + 1);
                    for (let i = 0; i < this.props.counter; i++) {
                        allProducts.push(allGoods[i]);
                    }
                    this.props.setGoods(allProducts);
                }}}>+ Item
                </div>
            </h1>
        </div>;
    }
}

const mapStateToProps = ({[COMPONENT_KEY_NAME]: state}: RootState) => ({
// const mapStateToProps = ({ 'cosmetic': state }: RootState) => ({
    ...ownSettings, ...state
});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => ({
    setUser: bindActionCreators(crosswordActions.setUser, dispatch),
    setName: bindActionCreators(crosswordActions.setName, dispatch),
    setGoods: bindActionCreators(crosswordActions.setGoods, dispatch),
    setCounter: bindActionCreators(crosswordActions.setCounter, dispatch),
    load: bindActionCreators(load, dispatch),
    incrementAsync: bindActionCreators(incrementAsync, dispatch),
});


type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
export default connect(mapStateToProps, mapDispatchToProps)(CrosswordComponent);
