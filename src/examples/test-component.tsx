import React from 'react';

interface TestComponentProps {
  name: string
}


class TestComponent extends React.Component<TestComponentProps> {
  render() {
    return <h1>TestComponent::Hello {this.props.name}</h1>;
  }
}

export default TestComponent;