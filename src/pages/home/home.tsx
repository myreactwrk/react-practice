import React from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import {RootState, useAppDispatch} from "src/main/store";
import api from 'src/api/api';
import { AxiosResponse } from 'axios';
import {homeActions, incrementAsync, loadCrossword, selectAddress, selectUser} from "./home.slice";
import {AnyAction, bindActionCreators, Dispatch} from "redux";
import {randomString} from "../../utils/misc";
// import {useLoadQuery} from "./home.api";


interface ComponentProps extends StateProps, DispatchProps {}

class HomeComponent extends React.Component<ComponentProps> {
  componentDidMount() {
    console.log('MOUNT...')
    console.log("############ MOUNT props", this.props);
    this.props.load('xxxx');
  }

  onClick = () => {
    this.props.xxxsetUser('YES!!! TEST::' + randomString('UU') );
    // this.props.incrementAsync('ZZ::' + randomString('UU'));
    // this.props.loadCrossword('history,listing,conster,fufelesis')
  }

  render() {
    // const { data, error, isLoading } = useLoadQuery('hello,bablo,fuflo');
    const address = this.props.address;
    const user = this.props.user;
    const pages = this.props.pages;

    // @ts-ignore
    return <div>
      <h1>HOMEEEEE Address:: [{address}] === USER: [{user}]</h1>
      <div onClick={this.onClick}> TEST ME</div>
      {pages.map(page => <div key={page.name}><a href={page.link}>{page.name}</a></div>)}
    </div>;
  }
}

const mapStateToProps = ({ home: homeState }: RootState) => ({
  version: '001', ...homeState
});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => ({
    xxxsetUser:         bindActionCreators(homeActions.setUser, dispatch),
    load:               bindActionCreators(homeActions.load, dispatch),
    incrementAsync:     bindActionCreators(incrementAsync, dispatch),
    loadCrossword:      bindActionCreators(loadCrossword, dispatch),
  });


type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);
