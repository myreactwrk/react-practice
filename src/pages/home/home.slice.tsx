import { createAction, createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit'
import { createApi } from '@reduxjs/toolkit/query/react';
import type { RootState } from 'src/main/store'

import api from 'src/api/api';
import { COMPONENT_KEY_NAME } from "./home.common";
import AppLinks from "../../main/appLinks";

// @see https://redux-toolkit.js.org/api/createAsyncThunk

export const loadCrossword = createAsyncThunk(
  `${COMPONENT_KEY_NAME}/loadCrossword`,
  async (words: string, thunkAPI) => {
    const response = await api.xxxcrossword(words);
    return response.data
  }
)

interface InitialState {
  loading: 'idle' | 'pending' | 'succeeded' | 'failed'
  address: string,
  user: string,
  age: number,
  pages: Array<Record<string, string>>
}

const initialState: InitialState = {
  loading: 'idle',
  address: 'ABC 123',
  user: "ROMMM-AAAA",
  age: 666,
  pages: [
    { 'name': 'Home',        link: `http://localhost:3000${AppLinks.PAGE_HOME}` },
    { 'name': 'DOCS',        link: `http://localhost:3000${AppLinks.PAGE_DOCUMENTS_EXAMPLES}` },
    { 'name': 'Provider',    link: `http://localhost:3000${AppLinks.PAGE_PROVIDER}` },
    { 'name': 'Cosmetic',   link: `http://localhost:3000${AppLinks.PAGE_COSMETIC}` },
    { 'name': 'Visual',      link: `http://localhost:3000${AppLinks.PAGE_VISUAL_EXAMPLE}` }

  ]
}

export const homeSlice = createSlice({
  name: COMPONENT_KEY_NAME,
  initialState,
  reducers: {
    setUser(state, action) {
      console.log("########### XXXX setUser action::", action)
      console.log("########### XXXX setUser action.payload", action.payload)
      state.user = action.payload;
    },
    load(state, action) {
      console.log("########### LOADDDD", action.payload)
    },
    updateAge(state, action) {
      state.age = action.payload.age;
      console.log("########### SAVE", action.payload)
    }
  },
  extraReducers: (builder) => {
    builder.addCase(loadCrossword.fulfilled, (state, action) => {
      console.log("################ action.payload::", action.payload)
      state.address = 'action.payload';
    })
  }
});



export const incrementAsync = (name) => (dispatch) => {
  setTimeout(() => {
    dispatch(homeSlice.actions.setUser(name))
  }, 1000)
}


export const homeActions = homeSlice.actions;

export const selectAddress = (state: RootState) => state.home.address;
export const selectUser = (state: RootState) => state.home.user;

export default homeSlice.reducer
