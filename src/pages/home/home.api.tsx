import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const homeApi = createApi({
  reducerPath: 'homeApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://crossword.worddict.net/?words=' }),
  endpoints: (builder) => ({
    load: builder.query<string, string>({
      query: (words) => `${words}`,
    }),
  }),
})

// AUTO-GENERATED - based on the defined endpoints
// export const { useGetCrosswordQuery } = homeApi;
export const { useLoadQuery } = homeApi;
