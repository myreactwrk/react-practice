import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "./store";

import 'bootstrap/dist/css/bootstrap.min.css';

import { AppRouter } from "./router";
// import 'src/main/App.css';

const App = () =>
    <Provider store={store}>
        <>
        {/*<App/>*/}
        <Router>
            <AppRouter/>
        </Router>
        </>
    </Provider>


export default App;
