import React from "react";
import { useRoutes } from "react-router-dom";

import AppLinks from "./appLinks";

import HomeView from 'src/pages/home/home';
import VisualViewExamples from "../examples/visual/visual.view";
import ExamplesAndDocumentsList from '../examples/documents';
const ProviderView =    React.lazy(() => import('src/examples/provider/provider.view'));
const CosmeticView =   React.lazy(() => import('src/examples/cosmetic/cosmetic'));

export const AppRouter = () =>
  useRoutes([
    { path: AppLinks.PAGE_HOME,                 element: <HomeView /> },
    { path: AppLinks.PAGE_PROVIDER,             element: <ProviderView /> },
    { path: AppLinks.PAGE_COSMETIC,            element: <CosmeticView /> },
    { path: AppLinks.PAGE_VISUAL_EXAMPLE,       element: <VisualViewExamples /> },
    { path: AppLinks.PAGE_DOCUMENTS_EXAMPLES,   element: <ExamplesAndDocumentsList /> },
    { path: "/home", element: <HomeView /> },
]);
