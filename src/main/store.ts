import promiseMiddleware from 'redux-promise-middleware';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';

import homeReducer from 'src/pages/home/home.slice';
import { homeApi } from '../pages/home/home.api';
import crosswordReducer, { COMPONENT_KEY_NAME as CROSSWORD_KEY_NAME } from 'src/examples/cosmetic/cosmetic.slice';
import providerReducer from 'src/examples/provider/provider.slice';
import { authApi } from 'src/examples/auth/auth.api';

import logger from "./middleware/logger";



import { createReduxHistoryContext } from 'redux-first-history';
import { createBrowserHistory } from 'history';
import Environment from "./config/Environment";
import {setupListeners} from "@reduxjs/toolkit/query";



// const { createReduxHistory, routerMiddleware, routerReducer } = createReduxHistoryContext({
//   history: createBrowserHistory(),
//   reduxTravelling: Environment.isDev(),
//   savePreviousLocations: 1,
// })

const preloadedState = {
};

export const store = configureStore({
  reducer: {
    home: homeReducer,
    [CROSSWORD_KEY_NAME]: crosswordReducer,
    [homeApi.reducerPath]: homeApi.reducer,
    [authApi.reducerPath]: authApi.reducer,
    provider: providerReducer
  },
  preloadedState,
  enhancers: [],
  devTools: Environment.isDev(),
  middleware: getDefaultMiddleware =>
      getDefaultMiddleware().concat([
        promiseMiddleware,
        logger,
        homeApi.middleware,
        authApi.middleware
      ])
});

setupListeners(store.dispatch);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;

export const useAppDispatch = () => useDispatch<AppDispatch>()
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
