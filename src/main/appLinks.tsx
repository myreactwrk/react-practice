
export default class AppLinks {
    static PAGE_HOME = '/';
    static PAGE_PROVIDER = '/provider';
    static PAGE_COSMETIC = '/cosmetic';
    static PAGE_VISUAL_EXAMPLE = '/visual_example';
    static PAGE_DOCUMENTS_EXAMPLES = '/documentList';
};
