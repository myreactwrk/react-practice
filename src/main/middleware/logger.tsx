export const logger = store => next => action => {
  if (process.env.NODE_ENV !== 'production') {
    const { type, payload, meta } = action;

    console.groupCollapsed(type);
    console.log('Payload:', payload);
    console.log('Meta:', meta);

    console.info('dispatching', action);
    const result = next(action);
    console.log('next state', store.getState());

    console.groupEnd();
    return result;
  }
  return next(action);
};

export default logger;
