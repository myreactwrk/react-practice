
export const randomString = (prefix = '_'): string => prefix + Math.round(Math.random() * 1000000) + '';

export const typeIsString = (value: any): boolean => typeof value === 'string' || value instanceof String;

export const distinct = (arr: any[]): any[] => arr.filter((value, index) => arr.indexOf(value) === index);
export const difference = (base: any[], diff: any[]) => base.filter(x => !diff.includes(x));
export const commonValues = (base: any[], diff: any[]) => base.filter(x => diff.includes(x));
export const removeElement = (arr: any[], element: any) => arr.splice(arr.indexOf(element), 1);

